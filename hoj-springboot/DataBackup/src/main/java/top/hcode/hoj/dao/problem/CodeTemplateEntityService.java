package top.hcode.hoj.dao.problem;

import com.baomidou.mybatisplus.extension.service.IService;
import pojo.entity.problem.CodeTemplate;

public interface CodeTemplateEntityService extends IService<CodeTemplate> {
}
