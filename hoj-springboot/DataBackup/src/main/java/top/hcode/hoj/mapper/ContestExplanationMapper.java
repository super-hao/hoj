package top.hcode.hoj.mapper;

import pojo.entity.contest.ContestExplanation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Himit_ZH
 * @since 2020-10-23
 */
public interface ContestExplanationMapper extends BaseMapper<ContestExplanation> {

}
