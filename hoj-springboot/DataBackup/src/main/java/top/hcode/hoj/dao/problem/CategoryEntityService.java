package top.hcode.hoj.dao.problem;

import com.baomidou.mybatisplus.extension.service.IService;
import pojo.entity.problem.Category;


public interface CategoryEntityService extends IService<Category> {
}
