package top.hcode.hoj.dao.group;

import pojo.entity.discussion.Discussion;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author: LengYun
 * @Date: 2022/3/11 13:36
 * @Description:
 */
public interface GroupDiscussionEntityService extends IService<Discussion> {
}
