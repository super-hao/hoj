package top.hcode.hoj.dao.training;

import com.baomidou.mybatisplus.extension.service.IService;
import pojo.entity.training.MappingTrainingCategory;

public interface MappingTrainingCategoryEntityService extends IService<MappingTrainingCategory> {
}
