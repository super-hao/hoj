package top.hcode.hoj.dao.contest;


import com.baomidou.mybatisplus.extension.service.IService;
import pojo.entity.contest.ContestAnnouncement;

public interface ContestAnnouncementEntityService extends IService<ContestAnnouncement> {
}
