package top.hcode.hoj.dao.problem;

import com.baomidou.mybatisplus.extension.service.IService;
import pojo.entity.problem.Language;

public interface LanguageEntityService extends IService<Language> {
}
