package top.hcode.hoj.dao.user;

import com.baomidou.mybatisplus.extension.service.IService;
import pojo.entity.user.UserAcproblem;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Himit_ZH
 * @since 2020-10-23
 */
public interface UserAcproblemEntityService extends IService<UserAcproblem> {

}
