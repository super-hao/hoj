package top.hcode.hoj.dao.judge;

import com.baomidou.mybatisplus.extension.service.IService;

import pojo.entity.judge.JudgeServer;

public interface JudgeServerEntityService extends IService<JudgeServer> {

}
