package top.hcode.hoj.dao.user;

import com.baomidou.mybatisplus.extension.service.IService;
import pojo.entity.user.Session;

public interface SessionEntityService extends IService<Session> {

    public void checkRemoteLogin(String uid);

}
